DROP DATABASE IF EXISTS lez_2_supermercato_alter;
CREATE DATABASE lez_2_supermercato_alter;
USE lez_2_supermercato_alter;

CREATE TABLE tessera (
	id INTEGER NOT NULL AUTO_INCREMENT,
    num_tessera TEXT NOT NULL,
    nome_negozio VARCHAR(50) NOT NULL,
    punti INT(2) DEFAULT 0,
    PRIMARY KEY (id)
);

CREATE TABLE cliente (
	id INTEGER NOT NULL AUTO_INCREMENT,
    nome VARCHAR(150) NOT NULL,
    cognome VARCHAR(150) NOT NULL,
    cod_fis VARCHAR(16) NOT NULL UNIQUE,
    PRIMARY KEY(id)
);
ALTER TABLE tessera ADD COLUMN cli_rif INTEGER NOT NULL;
ALTER TABLE tessera ADD FOREIGN KEY (cli_rif) REFERENCES cliente(id) ON DELETE CASCADE;




