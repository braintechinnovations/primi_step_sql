-- CREATE DATABASE lez_1_scuola;
-- USE lez_1_scuola;

/*
CREATE TABLE studente (
    matricola VARCHAR(10) NOT NULL, 
	nome VARCHAR(150) NOT NULL,
    cognome VARCHAR(150) NOT NULL,
    PRIMARY KEY(matricola)
);
*/

INSERT INTO studente (matricola, nome, cognome) VALUES
("AB123456", "Giovanni", "Pace"),
("AB123457", "Mario", "Rossi"),
("AB123458", "Valeria", "Verdi"),
("AB123459", "Marta", "Occhi");

/* OPERAZIONE NON POSSIBILE */
/* INSERT INTO studente (matricola, nome, cognome) VALUES
("AB123456", "Ciccio", "Pasticcio"); */

INSERT INTO studente (matricola, nome, cognome) VALUES
("AB123460", "Giovanni", "Cicinelli");

-- QL
SELECT nome, cognome, matricola 
	FROM studente 
    WHERE nome = "Giovanni";
    
-- DML
DELETE 
	FROM studente 
    WHERE matricola = "AB123460";

-- DML
INSERT INTO studente (matricola, nome, cognome) VALUES
("", "Giovanni", "Cicinelli");

SELECT * FROM studente;

INSERT INTO studente (matricola, nome, cognome) VALUES
("", "Mariangela", "Fantozzi");

