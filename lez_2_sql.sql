/*
	Creare un database chiamato "Rubrica telefonica", all'interno di questo database saranno salvati i seguenti dati:
    - Nome
    - Cognome
    - Telefono
    - Eta
    - Città
    - Codice Fiscale
    
    Inserire almeno 6 record
    Effettuare le seguenti ricerche
    - Ricerca per nome
    - Ricerca per Città
    - Ricerca per età minore di 60 anni
*/

-- CREATE DATABASE lez_2_rubrica_telefonica;
-- USE lez_2_rubrica_telefonica;

DROP TABLE IF EXISTS rubrica;
CREATE TABLE rubrica (
	id INTEGER NOT NULL AUTO_INCREMENT,
    nome VARCHAR(150) NOT NULL,
    cognome VARCHAR (150) NOT NULL,
    telefono VARCHAR (150),
    eta INTEGER CONSTRAINT eta_compatibile CHECK (eta >= 0 AND eta <= 120),
    citta VARCHAR (150) DEFAULT "n.d.",
    cod_fis VARCHAR (16) NOT NULL UNIQUE,
    PRIMARY KEY(id)
);

INSERT INTO rubrica (nome, cognome, telefono, eta, citta, cod_fis) VALUES 
("Giovanni", "Pianella", "123456", 12, "Roma", "C123"),
("Giorgio", "Rota", "123458", 85, null, "C124"),
("Maria", "Rossi", "789456", 50, "Roma", "C125"),
("Valeria", "Verdi", "4565646", 60, null, "C789"),
("Elisa", "Viola", "897987", 35, "Roma", "C963"),
("Marco", "Marchetti", "654654", 80, null, "C658");

INSERT INTO rubrica (nome, cognome, telefono, eta, cod_fis) VALUES 
("Marco", "Marchetti", "654654", 80, "C858");

INSERT INTO rubrica (citta, nome, cognome, telefono, eta, cod_fis) VALUES 
("", "Ciccio", "Pasticcio", "654654", 80, "C868");

SELECT * FROM rubrica;

-- Rapporto di equivalenza tra Null e campo vuoto
-- SELECT * FROM rubrica WHERE citta <> "";			-- Include sia i NULL che la stringa vuota
-- SELECT * FROM rubrica WHERE citta IS NOT NULL;	-- Include tutti i pieni e "stringhe vuote"
-- SELECT * FROM rubrica WHERE citta IS NULL;	

