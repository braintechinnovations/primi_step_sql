DROP DATABASE IF EXISTS lez_4_funzioni_avanzate;
CREATE DATABASE lez_4_funzioni_avanzate;
USE lez_4_funzioni_avanzate;

CREATE TABLE prodotti (
	ProdottoID INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nom_art VARCHAR(150) NOT NULL, 
    cod_art VARCHAR(20) NOT NULL,
    cat_art VARCHAR(150) NOT NULL,
    qua_art INTEGER DEFAULT 0,
    pre_art FLOAT DEFAULT 0
);

INSERT INTO prodotti (nom_art, cod_art, cat_art, qua_art, pre_art) VALUES
("Lampadina 10W","LAMPA10","Illuminazione",150,12.50),
("Porta Lampada","PL10","Illuminazione",120,2.30),
("Cavo Elettrico 25m","CA25","Elettrodomestici",590,0.21),
("Asciuga Capelli","AC789","Elettrodomestici",2, 390.00),
("Feltrini Mobili","FEL01","Mobili",64,0.21);

INSERT INTO prodotti (nom_art, cod_art, cat_art, qua_art, pre_art) VALUES
("Sapone Marsiglia","SAP789","Bellezza Personale",150,12.50);

SELECT * FROM prodotti;

SELECT COUNT(qua_art) FROM prodotti;	-- Contatore di ogni singola RIGA
SELECT SUM(pre_art) FROM prodotti;
SELECT AVG(pre_art) FROM prodotti;

SELECT MIN(pre_art) FROM prodotti;		-- Valore massimo/minimo
SELECT MAX(pre_art) AS "Prezzo massimo" FROM prodotti;

SELECT DISTINCT cat_art AS "Categoria" FROM prodotti;		-- Selezione di elementi distinti e quindi non ripetuti!
SELECT * FROM prodotti WHERE cat_art IN ("Mobili", "Illuminazione");

SELECT cat_art, COUNT(cat_art) AS "Conteggio", SUM(qua_art) AS pezzi_rimanenti
	FROM prodotti 
    -- WHERE pre_art > 10
    GROUP BY cat_art;
    -- HAVING pezzi_rimanenti < 70;
    -- HAVING cat_art <> "Elettrodomestici";