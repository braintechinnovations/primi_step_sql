DROP DATABASE IF EXISTS lez_4_transactions;
CREATE DATABASE lez_4_transactions;
USE lez_4_transactions;
SET autocommit = OFF;			-- Disabilita l'auto COMMIT

CREATE TABLE prodotti (
	ProdottoID INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nom_art VARCHAR(150) NOT NULL, 
    cod_art VARCHAR(20) NOT NULL,
    cat_art VARCHAR(150) NOT NULL,
    qua_art INTEGER DEFAULT 0,
    pre_art FLOAT DEFAULT 0
);

CREATE TABLE registro(
	LogID INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nom_ope VARCHAR(10) NOT NULL,
    dat_ope DATETIME DEFAULT CURRENT_TIMESTAMP
);

START TRANSACTION;
-- 1. Salvo lo stato del database prima di effettuare le modifiche
	INSERT INTO prodotti (nom_art, cod_art, cat_art, qua_art, pre_art) VALUE ("Lampadina 10W","LAMPA10","Illuminazione",150,12.50);
    INSERT INTO registro (nom_ope) VALUE ("INS");
	INSERT INTO prodotti (nom_art, cod_art, cat_art, qua_art, pre_art) VALUE ("Porta Lampada","PL10","Illuminazione",120,2.30);
    INSERT INTO registro (nom_ope) VALUE ("INS");
	INSERT INTO prodotti (nom_art, cod_art, cat_art, qua_art, pre_art) VALUE ("Asciuga Capelli","AC789","Elettrodomestici",2, 390.00);
    INSERT INTO registro (nom_ope) VALUE ("INS");
	INSERT INTO prodotti (nom_art, cod_art, cat_art, qua_art, pre_art) VALUE ("Feltrini Mobili","FEL01","Mobili",64,0.21);
    INSERT INTO registro (nom_ope) VALUE ("INS");
-- 2. Provo ad effettuare le modifiche
COMMIT;		
-- 3. Guardo il risultato delle modifiche
-- 3.1. SE: Le modifiche sono andate a buon fine e non ho ricevuto nessun errore, OK! Il nuovo stato del database è quello con le modifiche
-- 3.2. ALTRIMENTI: Torno al database allo stato del punto 1 (ROLLBACK)


SELECT * FROM prodotti;
SELECT * FROM registro;