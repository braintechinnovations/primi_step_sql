DROP DATABASE IF EXISTS lez_2_universita;
CREATE DATABASE lez_2_universita;
USE lez_2_universita;

CREATE TABLE studente (
	StudenteID INTEGER NOT NULL AUTO_INCREMENT,
    nome VARCHAR(150) NOT NULL,
    cognome VARCHAR(150) NOT NULL,
    matricola VARCHAR(20) NOT NULL UNIQUE,
    PRIMARY KEY (StudenteID)
);


CREATE TABLE esame (
	EsameID INTEGER NOT NULL AUTO_INCREMENT,
    titolo VARCHAR(150) NOT NULL,
    data_esame DATETIME,
    -- professore VARCHAR(150),
    PRIMARY KEY (EsameID)
);

INSERT INTO studente (nome, cognome, matricola) VALUES 
("Giovanni", "Pace", "123456"),
("Mario", "Rossi", "123457"),
("Valeria", "Verdi", "123458");
-- SELECT * FROM studente;

INSERT INTO esame (titolo, data_esame) VALUES
("FISICA I","2021-02-10 09:30"),
("FISICA I","2021-02-25 09:30"),
("ANALISI I","2021-02-23 10:15"),
("ANALISI I","2021-03-30 10:15"),
("GEOMETRICA I","2021-03-15 11:10");
-- SELECT * FROM esame;

CREATE TABLE Iscrizioni_Studente_Esame (
	studente_id INTEGER NOT NULL,
    esame_id INTEGER NOT NULL,
    data_iscrizione DATETIME NOT NULL,
    PRIMARY KEY (studente_id, esame_id),
    -- UNIQUE(studente_id, esame_id),
    FOREIGN KEY (studente_id) REFERENCES studente(StudenteID),
    FOREIGN KEY (esame_id) REFERENCES esame(EsameID)
);

INSERT INTO Iscrizioni_Studente_Esame (studente_id, esame_id, data_iscrizione) VALUES
(1,	1, "2020-12-30 09:10"),
(1,	3, "2020-12-30 09:11"),
(1,	5, "2020-12-30 09:12"),
(3,	1, "2020-12-30 09:13"),
(3,	4, "2020-12-30 09:14"),
(3,	5, "2020-12-30 09:15");

/*
SELECT * 
	FROM studente
    JOIN Iscrizioni_Studente_Esame ON studente.StudenteID = Iscrizioni_Studente_Esame.studente_id
    JOIN esame ON Iscrizioni_Studente_Esame.esame_id = esame.EsameID
    WHERE studente.nome = "Giovanni" AND studente.cognome = "Pace";
    
SELECT titolo AS ESAME, data_esame AS "DATA", matricola AS MATRICOLA
	FROM esame
    JOIN Iscrizioni_Studente_Esame ON esame.EsameID = Iscrizioni_Studente_Esame.esame_id
    JOIN studente ON Iscrizioni_Studente_Esame.studente_id = studente.StudenteID
    WHERE titolo = "ANALISI I" AND data_esame = "2021-03-30 10:15:00";
*/
/* 
	SE VOLESSI GESTIRE LE INFORMAZIONI SUI PROFESSORI CHE SUPERVISIONANO UN ESAME? 
*/

CREATE TABLE professore (
	ProfessoreID INTEGER NOT NULL AUTO_INCREMENT,
    Nome VARCHAR(150) NOT NULL,
    Cognome VARCHAR(150) NOT NULL,
    Matricola VARCHAR(20) NOT NULL UNIQUE,
    PRIMARY KEY (ProfessoreID)
);

INSERT INTO professore (Nome, Cognome, Matricola) VALUES
("Fabio", "Camilli", "CA1234"), 
("Antonio", "Mecozzi", "AC178"), 
("Anna", "Berardi", "AC7895");
-- SELECT * FROM professore;

CREATE TABLE Supervisiona_Esame_Professore (
	esame_id INTEGER NOT NULL,
    professore_id INTEGER NOT NULL,
    PRIMARY KEY (esame_id, professore_id),
    FOREIGN KEY (esame_id) REFERENCES esame(EsameID),
    FOREIGN KEY (professore_id) REFERENCES professore(ProfessoreID)
);

-- SELECT * FROM esame;
-- SELECT * FROM professore;

INSERT INTO Supervisiona_Esame_Professore(esame_id, professore_id) VALUES
(1, 1),
(1, 3),
(2, 1),
(2, 3),
(3, 2),
(4, 2),
(5, 1),
(5, 2),
(5, 3);

/*
SELECT *
	FROM esame
    JOIN Supervisiona_Esame_Professore ON esame.EsameID = Supervisiona_Esame_Professore.esame_id
    JOIN professore ON Supervisiona_Esame_Professore.professore_id = professore.ProfessoreID
    WHERE titolo LIKE "%FISICA%" ORDER BY data_esame DESC;
    */

SELECT *
	FROM studente
    JOIN Iscrizioni_Studente_Esame ON studente.StudenteID = Iscrizioni_Studente_Esame.studente_id
    JOIN esame ON Iscrizioni_Studente_Esame.esame_id = esame.EsameID
    JOIN Supervisiona_Esame_Professore ON esame.EsameID = Supervisiona_Esame_Professore.esame_id
    JOIN professore ON Supervisiona_Esame_Professore.professore_id = professore.ProfessoreID
    WHERE studente.matricola = 123456;
    
SELECT * 
	FROM professore
    JOIN Supervisiona_Esame_Professore ON professore.ProfessoreID = Supervisiona_Esame_Professore.professore_id
    JOIN esame ON Supervisiona_Esame_Professore.esame_id = esame.EsameID
    JOIN Iscrizioni_Studente_Esame ON esame.EsameID = Iscrizioni_Studente_Esame.esame_id
    JOIN studente ON Iscrizioni_Studente_Esame.studente_id = studente.StudenteID
    WHERE studente.matricola = 123456;
    
-- SELECT COUNT(nome) AS ContatoreStudenti FROM studente; 
-- SELECT COUNT(titolo) AS ContatoreEsami FROM esame; 

SELECT * FROM studente;