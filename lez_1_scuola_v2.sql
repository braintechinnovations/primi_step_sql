-- CREATE DATABASE lez_1_scuola_auto_in;
-- USE lez_1_scuola_auto_in;

/*
CREATE TABLE studente (
	id INTEGER AUTO_INCREMENT,
	matricola VARCHAR(10) DEFAULT "",
    nome VARCHAR(150) NOT NULL,
    cognome VARCHAR(150) NOT NULL,
    PRIMARY KEY(id)
);

INSERT INTO studente(matricola, nome, cognome) VALUES
("AB123456", "Giovanni", "Pace"),
("AB123457", "Mario", "Rossi"),
("AB123458", "Valeria", "Verdi"),
("AB123459", "Marta", "Occhi");

INSERT INTO studente (matricola, nome, cognome) VALUES
("", "Ciccio", "Pasticcio");

INSERT INTO studente (matricola, nome, cognome) VALUES
("", "Pastrocchio", "Cicinelli");

SELECT * FROM studente;

DELETE
	FROM studente
    WHERE id = 2;
    
SELECT * FROM studente;

INSERT INTO studente (id, matricola, nome, cognome) VALUES
(51, "", "Valeriona", "Marini");
SELECT * FROM studente;

INSERT INTO studente (matricola, nome, cognome) VALUES
("", "Pastrocchio", "Cicinelli");
SELECT * FROM studente;

INSERT INTO studente (matricola, nome, cognome) VALUES
("", "Giorgio", "Matrota");
SELECT * FROM studente;
*/

DROP TABLE IF EXISTS studente;
CREATE TABLE studente (
	id INTEGER AUTO_INCREMENT,
	matricola VARCHAR(10) DEFAULT "",
    nome VARCHAR(150) NOT NULL,
    cognome VARCHAR(150) NOT NULL,
    cod_fis VARCHAR(16) NOT NULL UNIQUE,
	PRIMARY KEY(id)
);

INSERT INTO studente(matricola, nome, cognome, cod_fis) VALUES
("AB123456", "Giovanni", "Pace", "PCA"),
("AB123457", "Mario", "Rossi", "TRE"),
("AB123458", "Valeria", "Verdi", "YFE"),
("AB123459", "Marta", "Occhi", "KJH");
SELECT * FROM studente;

INSERT INTO studente (matricola, nome, cognome, cod_fis) VALUES
("", "Valeriona", "Marini", "TRE");
SELECT * FROM studente;