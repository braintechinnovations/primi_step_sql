DROP DATABASE IF EXISTS lez_2_supermercato;
CREATE DATABASE lez_2_supermercato;
USE lez_2_supermercato;

CREATE TABLE cliente (
	id INTEGER NOT NULL AUTO_INCREMENT,
    nome VARCHAR(150) NOT NULL,
    cognome VARCHAR(150) NOT NULL,
    cod_fis VARCHAR(16) NOT NULL UNIQUE,
    PRIMARY KEY(id)
);

CREATE TABLE tessera (
	id INTEGER NOT NULL AUTO_INCREMENT,
    num_tessera TEXT NOT NULL,
    nome_negozio VARCHAR(50) NOT NULL,
    punti INT(2) DEFAULT 0,
    cli_rif INTEGER NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (cli_rif) REFERENCES cliente(id) ON DELETE CASCADE
);

INSERT INTO cliente (nome, cognome, cod_fis) VALUES
("Giovanni", "Pace", "C123"),
("Mario", "Rossi", "C321"),
("Valeria", "Verdi", "C789");

INSERT INTO tessera (num_tessera, nome_negozio, cli_rif) VALUES
("1234567489", "Conad", 1),
("AC789456456", "Coop", 2),
("987654785TT", "Tigotà", 1);

INSERT INTO cliente (nome, cognome, cod_fis) VALUES
("Giorgio", "Mastrota", "C788");
INSERT INTO cliente (nome, cognome, cod_fis) VALUES
("Giorgio Mario", "Mastrota", "C787");

-- INSERT INTO tessera (num_tessera, nome_negozio, cli_rif) VALUES
-- ("987654785TT", "Tuodì", 789);

SELECT * FROM tessera;
SELECT * FROM cliente WHERE nome = "Giorgio";

-- Introduzione del LIKE
SELECT * FROM cliente WHERE nome LIKE "Gio%";	-- Nessun limite a destra
SELECT * FROM cliente WHERE nome LIKE "%Mario"; -- Nessun limite a sinistra
SELECT * FROM cliente WHERE nome LIKE "%io%";	-- Ricerca contenuto non limitato


SELECT *
	FROM tessera
    JOIN cliente ON tessera.cli_rif = cliente.id;

SELECT tessera.id AS TESSERA_ID, cliente.id AS CLIENTE_ID, nome, cognome, punti, num_tessera
	FROM tessera
    JOIN cliente ON tessera.cli_rif = cliente.id;

-- Funzione inversa

SELECT * FROM cliente;
SELECT * FROM tessera;

SELECT *
	FROM cliente
	RIGHT JOIN tessera ON cliente.id = tessera.cli_rif;

-- ELIMINAZIONE:
SELECT * FROM cliente;
DELETE FROM cliente WHERE id = 6;

-- SET SQL_SAFE_UPDATES = 0;

-- MODIFICA
UPDATE cliente SET 
	nome = "Mariangela",
    cognome = "Fantozzi"
    WHERE id = 2;


-- DELETE FROM tessera WHERE id = 1;
-- DELETE FROM tessera WHERE id = 3;
DELETE FROM cliente WHERE id = 1;
SELECT * FROM tessera;
