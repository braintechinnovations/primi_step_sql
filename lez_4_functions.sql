DROP DATABASE IF EXISTS lez_4_functions;
CREATE DATABASE lez_4_functions;
USE lez_4_functions;

CREATE TABLE carta_credito(
	CartaID INTEGER AUTO_INCREMENT PRIMARY KEY,
    num_carta VARCHAR(16) NOT NULL UNIQUE,
    int_carta VARCHAR(250) NOT NULL,
    mm_scadenza INTEGER(2) CONSTRAINT verifica_mese CHECK (mm_scadenza > 0 AND mm_scadenza <=12),
    aa_scadenza INTEGER(4) CONSTRAINT verifica_anno CHECK (aa_scadenza > 1970 AND aa_scadenza <= 2100),
    cvv_carta INTEGER(3) NOT NULL,
    limite_credito INTEGER DEFAULT 0
);

INSERT INTO carta_credito(num_carta, int_carta, mm_scadenza, aa_scadenza, cvv_carta, limite_credito) VALUES 
("1234567812345678", "Giovanni Pace", "01", "2022", 896, 3000),
("2234567812345678", "Mario Rossi", "01", "2022", 231, 5000),
("3234567812345678", "Valeria Verdi", "01", "2022", 543, 10000);

SELECT * FROM carta_credito;

-- Funzione che prende in input il credito della persona in intero e restituisce il livello "BRONZE/SILVER/GOLD"
DELIMITER $$
CREATE FUNCTION verificaLivelloCreditizio(limite_cred INTEGER)
RETURNS VARCHAR(20)
DETERMINISTIC
BEGIN
	DECLARE risultato VARCHAR(20);
    
    IF (limite_cred > 0 AND limite_cred <= 3000) THEN
		SET risultato = "BRONZE";
	ELSEIF (limite_cred > 3000 AND limite_cred <= 5000) THEN
		SET risultato = "SILVER";
	ELSE
		SET risultato = "GOLD";
	END IF;
    
    RETURN(risultato);
END$$


SELECT num_carta, int_carta, limite_credito, verificaLivelloCreditizio(limite_credito) FROM carta_credito;

/*
DELIMITER $$
CREATE PROCEDURE verificaLivelloCreditizio(
	IN limite_cred INTEGER,
    OUT risultato VARCHAR(20)
)
BEGIN
    IF (limite_cred > 0 AND limite_cred <= 3000) THEN
		SET risultato = "BRONZE";
	ELSEIF (limite_cred > 3000 AND limite_cred <= 5000) THEN
		SET risultato = "SILVER";
	ELSE
		SET risultato = "GOLD";
	END IF;
END$$

CALL verificaLivelloCreditizio(3000, @var_risultato);
SELECT @var_risultato;
*/

