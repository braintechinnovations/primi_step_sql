DROP DATABASE IF EXISTS lez_3_ecommerce;
CREATE DATABASE lez_3_ecommerce;
USE lez_3_ecommerce;

CREATE TABLE Cliente(
	ClienteID INTEGER NOT NULL AUTO_INCREMENT,
    cod_cli VARCHAR(20) NOT NULL UNIQUE,
    nome VARCHAR(150) NOT NULL,
    cognome VARCHAR(150) NOT NULL,
    PRIMARY KEY (ClienteID)
);

CREATE TABLE Categoria (
	CategoriaID INTEGER NOT NULL AUTO_INCREMENT,
    nome VARCHAR(150) NOT NULL,
    descrizione VARCHAR(150),
    PRIMARY KEY (CategoriaID)
);

CREATE TABLE Prodotto(
	ProdottoID INTEGER NOT NULL AUTO_INCREMENT,
    cod_pro VARCHAR(20) NOT NULL UNIQUE,
    nome varchar(150) NOT NULL,
    descrizione TEXT,
    prezzo FLOAT,
    PRIMARY KEY(ProdottoID)
);

CREATE TABLE Prodotto_Categoria(
	prodotto_id INTEGER NOT NULL,
    categoria_id INTEGER NOT NULL,
    PRIMARY KEY (prodotto_id, categoria_id),
    FOREIGN KEY (prodotto_id) REFERENCES prodotto(ProdottoID) ON DELETE CASCADE,
    FOREIGN KEY (categoria_id) REFERENCES categoria(CategoriaID) ON DELETE CASCADE
);

CREATE TABLE Indirizzo(
	IndirizzoID INTEGER NOT NULL,
    tipo_indirizzo VARCHAR(20) CONSTRAINT tipologia_indirizzo CHECK (tipo_indirizzo IN ("FAT", "SPE")),
    intestazione VARCHAR(400) NOT NULL,
	ind_via VARCHAR(250) NOT NULL,
    ind_cit VARCHAR(250) NOT NULL,
    ind_cap VARCHAR(10) NOT NULL,
	ind_pro VARCHAR(10) NOT NULL,
    partita_iva VARCHAR(20),
    codice_fiscale VARCHAR(16),
    PRIMARY KEY (IndirizzoID)
);


CREATE TABLE Ordine(
	OrdineID INTEGER NOT NULL AUTO_INCREMENT,
    num_ord VARCHAR(20) NOT NULL UNIQUE,
	dat_ord DATETIME DEFAULT NOW(),
    cli_rif INTEGER NOT NULL,
    ind_spe INTEGER NOT NULL,
    ind_fat INTEGER NOT NULL,
    PRIMARY KEY (OrdineID),
    FOREIGN KEY (cli_rif) REFERENCES cliente(ClienteID) ON DELETE CASCADE,
    FOREIGN KEY (ind_spe) REFERENCES indirizzo(IndirizzoID) ON DELETE CASCADE,
    FOREIGN KEY (ind_fat) REFERENCES indirizzo(IndirizzoID) ON DELETE CASCADE
);

CREATE TABLE Prodotto_Ordine(
	prodotto_id INTEGER NOT NULL,
    ordine_id INTEGER NOT NULL,
    quantita INTEGER DEFAULT 1,
    PRIMARY KEY(prodotto_id, ordine_id),
    FOREIGN KEY (prodotto_id) REFERENCES prodotto(ProdottoID) ON DELETE CASCADE,
    FOREIGN KEY (ordine_id) REFERENCES ordine(OrdineID) ON DELETE CASCADE
);

INSERT INTO categoria (nome) VALUES
("Illuminazione"),
("Complemento arredo"),
("Informatica"),
("Vettovaglie");

INSERT INTO Prodotto(cod_pro, nome, prezzo) VALUES
("LAMP1234", "Lampada VRBRD", 12.6),
("SDRA1234", "Sdraio SCRFBRD", 35.0);

SELECT * FROM categoria;
SELECT * FROM prodotto;

INSERT INTO Prodotto_Categoria (prodotto_id, categoria_id) VALUES 
(1, 1), (1, 2), (2, 2);

SELECT *
	FROM prodotto
    JOIN Prodotto_Categoria ON prodotto.ProdottoID = Prodotto_Categoria.prodotto_id
    JOIN categoria ON Prodotto_Categoria.categoria_id = categoria.CategoriaID;
    
-- DELETE FROM prodotto WHERE ProdottoID =  1;
-- DELETE FROM categoria WHERE CategoriaID =  2;
    
-- BREAK END: 16:40