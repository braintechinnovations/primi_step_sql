/*
Per una azienda di vendita di autoveicoli voglio memorizzare per ogni cliente 
le informazioni relative ai veicoli a lui venduti ed ovviamente tener traccia di un suo
documento di riconoscimento (patente).

L'obiettivo è quello di poter tracciare ogni veicolo venduto (e invenduto) e, per quelli
venduti, poter inviare un "reminder" sul tagliando da effettuare ogni 12 mesi.

Effettuare qualche inserimento all'interno del Database che mi permetta di conoscere:
- Tutti i veicoli invenduti
- Tutti i veicoli posseduti da una persona
- Conoscere tutti i veicoli che hanno la scadenza del tagliando da effettuare quest'anno
	ed i relativi proprietari

*/

DROP DATABASE IF EXISTS lez_2_concessionaria;
CREATE DATABASE lez_2_concessionaria;
USE lez_2_concessionaria;

CREATE TABLE cliente (
	id INTEGER NOT NULL AUTO_INCREMENT,
    nome VARCHAR(150) NOT NULL,
    cognome VARCHAR(150) NOT NULL,
    recapito VARCHAR(50) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE documento (
	id INTEGER NOT NULL AUTO_INCREMENT,
    -- nome VARCHAR(150) NOT NULL,
    -- cognome VARCHAR(150) NOT NULL,
    tipo_documento VARCHAR(10) CONSTRAINT tipologia_doc CHECK (tipo_documento IN ("PATENTE", "CI")),
    codice_documento VARCHAR(150) UNIQUE NOT NULL,
    data_rilascio DATE,
    data_scadenza DATE,
    colore_occhi VARCHAR(20),
    altezza FLOAT,
    cli_rif INTEGER NOT NULL UNIQUE,
    PRIMARY KEY (id),
    FOREIGN KEY (cli_rif) REFERENCES cliente(id) ON DELETE CASCADE
);

CREATE TABLE veicolo (
	id INTEGER NOT NULL AUTO_INCREMENT,
    targa VARCHAR(10) NOT NULL,
    telaio varchar(30) NOT NULL,
    modello  varchar(30) NOT NULL,
    marca varchar(30) NOT NULL,
    data_vendita DATE,
    cli_vendita INTEGER,
    UNIQUE(targa),
    UNIQUE(telaio),
    PRIMARY KEY(id),
    FOREIGN KEY (cli_vendita) REFERENCES cliente(id)
);

INSERT INTO cliente (nome, cognome, recapito) VALUES
("Giovanni", "Pace", "123456"),
("Mario", "Rossi", "987654"),
("Valeria", "Verdi", "654987");
SELECT * FROM cliente;

INSERT INTO documento (
                tipo_documento, 
                codice_documento, 
                data_rilascio, 
                data_scadenza,
                cli_rif
                ) VALUES
("PATENTE","PAT123456","2020-01-01","2020-09-01",1);
SELECT * FROM documento;

SELECT nome, cognome, tipo_documento, codice_documento 
	FROM cliente 
    JOIN documento ON cliente.id = documento.cli_rif
    WHERE nome = "Giovanni" AND cognome = "Pace";